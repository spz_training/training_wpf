using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp_Toturial
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Locker : INotifyPropertyChanged
        {

            public event PropertyChangedEventHandler PropertyChanged;
            public string locker_id { get; set; }
            public string name { get; set; }
            public byte status { get; set; }
            public string hardware_address { get; set; }
            public string block_name { get; set; }
            public int group { get; set; }
            public string size_id { get; set; }
            public string position { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string type { get; set; }
            public string board_address { get; set; }
            private int colorFormat;
            //public int ColorFormat  
            //{
            //    get => colorFormat;
            //    set
            //    {
            //        colorFormat = value;
            //        OnPropertyChanged("ColorFormat"); 
            //    }
            //}
            public int ColorFormat
            {
                get { return this.colorFormat; }
                set
                {
                    this.colorFormat = value;
                    OnPropertyChanged("ColorFormat");
                }
            }
            protected void OnPropertyChanged(string name)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
                }
            }

        }

        public class BlockUI : ICloneable, IDisposable
        {
            public int block_id { get; set; }

            public int BlockWidth { get; set; }

            public List<Locker> LockerUIs { get; set; }
            public object Clone()
            {
                return base.MemberwiseClone();
            }
            // Other managed resource this class uses.
            private Component component = new Component();
            // Track whether Dispose has been called.
            private bool disposed = false;
            // Pointer to an external unmanaged resource.
            private IntPtr handle;
            public BlockUI()
            {
                this.handle = (IntPtr)System.Runtime.InteropServices.GCHandle.Alloc(this);
            }

            public void Dispose()
            {
                Dispose(true);
                // This object will be cleaned up by the Dispose method.
                // Therefore, you should call GC.SupressFinalize to
                // take this object off the finalization queue
                // and prevent finalization code for this object
                // from executing a second time.
                GC.SuppressFinalize(this);
            }
            protected virtual void Dispose(bool disposing)
            {
                // Check to see if Dispose has already been called.
                if (!this.disposed)
                {
                    // If disposing equals true, dispose all managed
                    // and unmanaged resources.
                    if (disposing)
                    {
                        // Dispose managed resources.
                        component.Dispose();
                    }

                    // Call the appropriate methods to clean up
                    // unmanaged resources here.
                    // If disposing is false,
                    // only the following code is executed.
                    CloseHandle(handle);
                    handle = IntPtr.Zero;

                    // Note disposing has been done.
                    disposed = true;

                    System.Diagnostics.Debug.WriteLine("Dispose BlockUI ~");
                }
            }
            // Use interop to call the method necessary
            // to clean up the unmanaged resource.
            [System.Runtime.InteropServices.DllImport("Kernel32")]
            private extern static Boolean CloseHandle(IntPtr handle);

            // Use C# destructor syntax for finalization code.
            // This destructor will run only if the Dispose method
            // does not get called.
            // It gives your base class the opportunity to finalize.
            // Do not provide destructors in types derived from this class.
            ~BlockUI()
            {
                // Do not re-create Dispose clean-up code here.
                // Calling Dispose(false) is optimal in terms of
                // readability and maintainability.
                Dispose(false);
            }
        }
        public List<BlockUI> BlockLockerUIs { get; set; }
        public List<BlockUI> BlockUIs { get; set; }
        private async void DrawLocker(bool isRetireve = true)
        {

            try
            {
                List<Locker> locker = new List<Locker>();
                using (StreamReader sr = File.OpenText(@"C:\locker.json"))
                {
                    var obj = sr.ReadToEnd();
                    locker = JsonConvert.DeserializeObject<List<Locker>>(obj);
                    //DataContext = locker;
                }

                BlockUIs = (from ll in locker
                            group ll by ll.@group into ll
                            select new BlockUI { block_id = ll.Key, LockerUIs = ll.ToList() }).ToList();

                BlockUIs = BlockUIs.OrderBy(x => x.block_id).ToList();
                BlockUIs.ForEach(x => x.LockerUIs = x.LockerUIs.OrderBy(y => y.position).ToList());

                //var countBlock = BlockUIs.Count();

                BlockLockerUIs = BlockUIs;

                foreach (var block in BlockLockerUIs.OrderBy(x => x.block_id))
                {
                    //var lockerCount = block.LockerUIs.Count();
                    foreach (var lk in block.LockerUIs)
                    {
                        if (lk.locker_id == locker[0].locker_id)
                            lk.ColorFormat = 1; // => Green background
                        else
                            lk.ColorFormat = 2; // => Gray background

                    }
                }
                // Bidding data to XAML
                this.DataContext = this;
            }
            catch (Exception ex)
            {

            }
        }

        private List<Locker> lstLockerOpen = new List<Locker>();
        private void StpALocker_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            StackPanel stackPanel = sender as StackPanel;
            Locker locker = (Locker)stackPanel.Tag;
            if (locker.name.ToLower() == "screen")
            {
                return;
            }
            if (locker.ColorFormat == 3)
            {
                locker.ColorFormat = 2;
                lstLockerOpen.Remove(locker);
                return;
            }
            if (locker.ColorFormat == 2)
            {
                lstLockerOpen.Add(locker);

            }
            else
            {
                lstLockerOpen.Remove(locker);

            }
            if (lstLockerOpen.Count > 2)
            {
                lstLockerOpen[0].ColorFormat = 2;
                lstLockerOpen.RemoveAt(0);
            }

            foreach (var item in lstLockerOpen)
            {
                item.ColorFormat = 3;
            }
            locker.ColorFormat = locker.ColorFormat == 1 ? 2 : 1;
        }
        public MainWindow()
        {
            InitializeComponent();
            DrawLocker();
        }

    }
}
